@extends('layouts.parent')
<!--Content to serve-->
@section('killer')

    <!--<p>Welcome to our task app!!</p>-->

    <!--Jumbotron heading-->
    <div class="container-fluid">
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-3">TapP</h1>
                <p class="lead">Manage your tasks</p>
            </div>
        </div>

            <!--Carousel heading-->
            <div class="col-md-offset-2">
                <div class="col-md-10">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <img class="d-block img-fluid" src="\images\tapplogo.png" alt="First slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>Manage tasks</h3>
                                    <p>...</p>
                                </div>
                            </div>
                            <!--<div class="carousel-item">
                                <img class="d-block img-fluid" src="\images\tapplogo.png" alt="Second slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>Edit Tasks</h3>
                                    <p>...</p>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <img class="d-block img-fluid" src="\images\tapplogo.png" alt="Third slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <h3>Delete Tasks</h3>
                                    <p>...</p>
                                </div>
                            </div>-->
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>

            <!--Cards-->
            <div class="col-md-offset-1">
                    <div class="col-md-4">
                        <div class="card" style="width: 20rem;">
                            <!--<img class="card-img-top" src="..." alt="Card image cap">-->
                            <div class="card-block">
                                <h4 class="card-title">Add Task</h4>
                                <p class="card-text">At start, you are provided with a blank task sheet for you to populate. You are at liberty to publish whatever you want to</p>
                                <a href="{{route('add_task.create')}}" class="btn btn-primary">Add Task</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="card" style="width: 20rem;">

                            <div class="card-block">
                                <h4 class="card-title">Edit/Delete Task</h4>
                                <p class="card-text">Provides you with the ability to make changes on some pre-defined tasks set by the user. Making managing easy.</p>
                                <a href="#" class="btn btn-primary">Edit Task</a>
                            </div>
                        </div>
                    </div>

                        <div class="col-md-4">
                            <div class="card" style="width: 20rem;">

                                <div class="card-block">
                                    <h4 class="card-title">View All Tasks</h4>
                                    <p class="card-text">After you have created the tasks, you have the ability to view all tasks created making managing your tasks easier.</p>
                                    <a href="{{route('add_task.index')}}" class="btn btn-primary">View All Tasks</a>
                                </div>
                            </div>
                        </div>
                </div>
            <div class="col-md-12">
                <ul class="nav justify-content-end">
                    <li class="nav-item pull-right">
                        <a class="nav-link" href="#">Home</a>
                    </li>
                </ul>
            </div>
    </div>
@endsection